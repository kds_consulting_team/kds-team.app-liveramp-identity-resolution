# LiveRamp Identity Resolution application
AbiliTec was designed to solve some of the main challenges with customer and prospect data by returning people-based identifiers and metadata for your consumer records. Clients can use these outputs to connect fragmented data sets, move data without sending PII, and increase the accuracy of CRM files.

## Configuration
This component has configured to request against 'Match Endpoints'. The input data is taken through a sequence of matching steps utilizing various components of the PII unitl a match to a maintained AbiliTech ID is found.

## API Usage and Rate Limits

 - 50 calls per minute
 - If limit is exceed, your calls will fail and an error message will be returned until the minute is up and you are allowed another 50 calls

## Parameters

1. Client ID
2. Client Secret
3. Endpoint
    1. People
    2. Places
    3. Households
    4. Entities
4. Test Environment
    - If user wants to fetch related endpoint data from 'Test Environment'