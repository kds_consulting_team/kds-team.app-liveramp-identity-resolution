import logging
import sys
from abilitec.component import AbilitecComponent

# Environment setup
sys.tracebacklimit = 0

if __name__ == '__main__':

    c = AbilitecComponent()
    c.run()

    logging.info("Matching finished.")
