import logging
import sys
from kbc.client_base import HttpClientBase
from urllib.parse import urljoin, urlencode
from typing import List

BASE_URL_TEST = 'https://test.api.acxiom.com/v1/'
BASE_URL_PROD = 'https://us.identity.api.liveramp.com/v1/'

LOGIN_URL_TEST = 'https://login.myacxiom.com/api/v1/auth/oauth2/token'
LOGIN_URL_PROD = 'https://us.identity.api.liveramp.com/token'


class AbilitecClient(HttpClientBase):

    def __init__(self, client_id, client_secret, test_env=True):

        _def_header = {
            'Accept': 'application/json'
        }

        super().__init__(base_url=BASE_URL_TEST if test_env else BASE_URL_PROD, default_http_header=_def_header)
        self.par_client_id = client_id
        self.par_client_secret = client_secret
        self.par_test_env = test_env
        # self.par_base_url = base_url

        self.refreshAccessToken()

    def refreshAccessToken(self):

        url_access = LOGIN_URL_TEST if self.par_test_env else LOGIN_URL_PROD
        logging.info(f"Using {url_access} to log in.")
        body_access = {
            'client_id': self.par_client_id,
            'client_secret': self.par_client_secret,
            'grant_type': 'client_credentials'
        }

        headers_access = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        request_access = self.post_raw(url_access, headers=headers_access, data=body_access)
        sc_access = request_access.status_code

        if sc_access == 200:

            logging.debug("Access token obtained.")
            self.par_access_token = request_access.json()['access_token']
            self._auth_header = {
                'Authorization': f'Bearer {self.par_access_token}',
                'Accept': 'application/json'  # instead of format=json parameter
            }

        else:
            logging.error(f"Could not refresh access token. Received: {sc_access} - {request_access.text}.")
            sys.exit(1)

    def sendMatchRequest(self, endpoint, parameters):

        url_match = urljoin(self.base_url, f"{endpoint}/match")
        params_match = parameters

        req_match = self.get_raw(url_match, params_match)
        sc_match, js_match = req_match.status_code, req_match.json()

        if sc_match == 200:
            return js_match

        else:
            logging.error(f"Received: {sc_match} - {js_match}.")
            sys.exit(1)  # Hard fail for now, can be adjusted later

    def sendBatchMatchRequest(self, endpoint: str, record_ids: List, batch_list: List):

        url_batch = urljoin(self.base_url, 'batch/match')
        body_batch = []
        prefix_batch = f'/{endpoint}/match?'

        for r in batch_list:
            body_batch += [prefix_batch + urlencode(r)]

        # logging.debug(body_batch)

        req_batch = self.post_raw(url_batch, json=body_batch)
        sc_batch, js_batch = req_batch.status_code, req_batch.json()

        if sc_batch != 200:

            logging.exception("Could not batch match identity data.")
            logging.error(f"Received: {sc_batch} - {js_batch}")
            sys.exit(1)

        else:
            results = []

            for r_id, i in zip(record_ids, js_batch):
                if i['code'] == 200:
                    results += [i['document']]

                else:
                    logging.exception(f"Could not match data for records id {r_id}.")
                    logging.error(f"Received: {i['code']} - {i['document']}.")
                    logging.debug(f"Full response: {i}.")
                    sys.exit(1)

            return results
