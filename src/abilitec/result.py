import csv
import os
import json

FIELDS_RESULTS = [
    'type',
    'record_id',
    'extraction_date',
    'key',
    'value'
]
PK_RESULTS = [
    'type',
    'record_id',
    'extraction_date',
    'key'
]

FIELDS_RESULTS_RAW = ['record_id', 'result']
PK_RESULTS_RAW = ['record_id']


class AbilitecWriter:

    def __init__(self, data_out_path, incremental_load=True, raw=False):

        self.par_data_out_path = data_out_path
        self.par_table_path = os.path.join(self.par_data_out_path, 'results.csv')
        self.par_table_path_raw = os.path.join(self.par_data_out_path, 'results-raw.csv')
        self.par_incremental_load = incremental_load
        self.par_raw_write = raw

        self._createManifest()
        self._createWriter()

    def _createManifest(self):

        _template = {
            'incremental': self.par_incremental_load,
            'primary_key': PK_RESULTS,
            'columns': FIELDS_RESULTS
        }

        with open(self.par_table_path + '.manifest', 'w') as manFile:
            json.dump(_template, manFile)

        if self.par_raw_write is True:

            _template = {
                'incremental': self.par_incremental_load,
                'primary_key': PK_RESULTS_RAW,
                'columns': FIELDS_RESULTS_RAW
            }

            with open(self.par_table_path_raw + '.manifest', 'w') as manFile:
                json.dump(_template, manFile)

    def _flatten(self, obj):

        out = {}

        def flatten(x, name=''):
            if type(x) is dict:
                for a in x:
                    flatten(x[a], name + a + '.')
            elif type(x) is list:
                i = 0
                for a in x:
                    flatten(a, name + str(i) + '.')
                    i += 1
            else:
                out[name[:-1]] = x

        flatten(obj)
        return out

    def _createWriter(self):

        self.writer = csv.DictWriter(open(self.par_table_path, 'w'), fieldnames=FIELDS_RESULTS, restval='',
                                     extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)

        if self.par_raw_write is True:
            self.writer_raw = csv.DictWriter(open(self.par_table_path_raw, 'w'), fieldnames=FIELDS_RESULTS_RAW,
                                             restval='', extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)

    def writerow(self, rowDict):

        self.writer.writerow(rowDict)

    def writerow_raw(self, rowDict):

        self.writer_raw.writerow(rowDict)
