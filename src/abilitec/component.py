import csv
import glob
import json  # noqa
import logging
import os
import sys
from datetime import datetime
from typing import List
from kbc.env_handler import KBCEnvHandler
from abilitec.client import AbilitecClient
from abilitec.result import AbilitecWriter

APP_VERSION = '0.1.0'
BATCH_MAX_RECORDS = 100

KEY_CLIENTID = 'client_id'
KEY_CLIENTSECRET = '#client_secret'
KEY_ENDPOINT = 'endpoint'
KEY_TESTENV = 'test_environment'
KEY_INCREMENTALLOAD = 'incremental_load'
KEY_DEBUGMODE = 'debug_mode'

MANDATORY_PARAMS = [KEY_CLIENTID, KEY_CLIENTSECRET, KEY_ENDPOINT]


class AbilitecComponent(KBCEnvHandler):

    def __init__(self):

        super().__init__(mandatory_params=MANDATORY_PARAMS, log_level='DEBUG')
        logging.info(f"Running component version {APP_VERSION}.")
        self.validate_config(MANDATORY_PARAMS)
        self.current_date = datetime.date(datetime.now())

        self.param_client_id = self.cfg_params[KEY_CLIENTID]
        self.param_client_secret = self.cfg_params[KEY_CLIENTSECRET]
        self.param_endpoint = self.cfg_params[KEY_ENDPOINT]
        self.param_test_env = bool(self.cfg_params.get(KEY_TESTENV, False))
        self.param_incremental = bool(self.cfg_params.get(KEY_INCREMENTALLOAD, True))
        self.param_debug_mode = self.cfg_params.get(KEY_DEBUGMODE, False)

        self.client = AbilitecClient(client_id=self.param_client_id,
                                     client_secret=self.param_client_secret,
                                     test_env=self.param_test_env)

        self.writer = AbilitecWriter(self.tables_out_path, self.param_incremental, self.param_debug_mode)
        self.getInputTables()

    def getInputTables(self):

        input_tables = glob.glob(os.path.join(self.tables_in_path, '*.csv'))

        if len(input_tables) == 0:
            logging.error("No input tables specified.")
            sys.exit(1)

        tables_with_missing_id = []
        for t in input_tables:
            with open(t) as in_table:
                _rdr = csv.DictReader(in_table)

                if 'record_id' not in _rdr.fieldnames:  # using 'record_id' since 'id' is included in the API response
                    tables_with_missing_id += [os.path.basename(t)]

        if len(tables_with_missing_id) != 0:
            logging.error(f"Column \"record_id\" is missing in table(s): {tables_with_missing_id}.")
            sys.exit(1)

        self.var_input_tables = input_tables

    def flattenAndWriteData(self, record_ids: List, matched_data: List):

        for r_id, data in zip(record_ids, matched_data):
            for obj_type in data:
                obj_data = self.writer._flatten(data[obj_type])
                for data_pt in obj_data:

                    json_row = {
                        'type': obj_type,
                        'record_id': r_id,
                        'extraction_date': self.current_date,
                        'key': data_pt,
                        'value': obj_data[data_pt]
                    }
                    self.writer.writerow(json_row)

    def writeRawData(self, record_ids: List, data: List):

        for r_id, d in zip(record_ids, data):
            _out = {
                'record_id': r_id,
                'result': json.dumps(d)
            }

            self.writer.writerow_raw(_out)

    def run(self):

        for table in self.var_input_tables:
            logging.info('Parsing records from [{}]'.format(table))
            with open(table) as in_table:

                _rdr = csv.DictReader(in_table)
                table_name = table.split('/tables/')[1]
                num_of_rows = sum(1 for row in _rdr)
                logging.info('[{}] - Total records: {}'.format(table_name, num_of_rows))

                in_table.seek(0)
                next(in_table)

                count = 0

                records_to_parse = []
                record_ids = []

                for row in _rdr:
                    count += 1

                    record_ids += [row['record_id']]
                    del row['record_id']
                    records_to_parse += [row.copy()]

                    if count % BATCH_MAX_RECORDS == 0 or count == num_of_rows:
                        matched_data = self.client.sendBatchMatchRequest(endpoint=self.param_endpoint,
                                                                         record_ids=record_ids,
                                                                         batch_list=records_to_parse)
                        self.flattenAndWriteData(record_ids, matched_data)

                        if self.param_debug_mode is True:
                            self.writeRawData(record_ids, matched_data)

                        records_to_parse = []
                        record_ids = []

                    else:
                        continue

                    if count % BATCH_MAX_RECORDS == 0 or count == num_of_rows:
                        logging.info('[{}] - Records parsed: {} - Records remaining: {}'.format(table_name, count,
                                                                                                num_of_rows-count))
